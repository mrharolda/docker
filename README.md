### Setup
1. Kopieer `.env.example` naar `.env`
1. Verander `COMPOSE_PROJECT_NAME`
1. Kopieer `docker-compose.example.yml` naar `docker-compose.yml`
1. Uncomment alles wat je nodig hebt in `docker-compose.yml`
1. `docker-composer up -d`

### TODO
1. Fatsoenlijke documentatie
1. Meerdere php versies
1. Drush
1. ...