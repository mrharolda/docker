#!/bin/bash
d7i10n() {
  local PROJECT="$1"
  local GIT_ROOT=$(git rev-parse --show-toplevel)
  local TRANSLATION_PATH=$(find . -path "*/translations/custom" -print -quit)

  # We should validate more.
  if [ -z "$PROJECT" ]; then
    echo "Please specify project."
    return 1
  fi

  # D7 specific.
  local LANG=$(drush eval "\$lang = locale_language_list('name', TRUE); unset(\$lang['en']); echo implode(' ', array_keys(\$lang));")
  local INFO_FILE=$(find . -name "$PROJECT.info" -print -quit)
  local VERSION=$(grep ^version "$INFO_FILE" | cut -d" " -f3)

  # Generate the po files.
  for _LANG in $LANG; do
    drush potx single --language="$_LANG" --translations=1 --modules="$PROJECT" # Also D7.
    local GENERAL_POT=$(find "$GIT_ROOT" -name "general.pot" -print -quit)
    sed -ri 's/([a-zA-Z\/-_\.]+):[0-9;]+/\1/g' "$GENERAL_POT" #TODO: only apply in lines beginning with '#: '
    mv "$GENERAL_POT" "$TRANSLATION_PATH/$PROJECT-$VERSION.$_LANG.po"
  done
}

d7up() {
  local GIT_ROOT=$(git rev-parse --show-toplevel)
  cd "$GIT_ROOT" && \
  git pull && \
  composer install && \
#  composer drupal:l10n && \
  drush l10n-update-refresh && \
  drush l10n-update --mode=replace && \
  drush updb && \
  drush fra -y && \
  drush cc all
  security-checker security:check composer.lock
}